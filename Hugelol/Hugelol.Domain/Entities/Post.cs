﻿namespace Hugelol.Domain.Entities
{
    public class Post
    {
        public string Bot { get; set; }

        public string BotCommented { get; set; }

        public string Cleaned { get; set; }

        public string Color { get; set; }

        public string CommentAmount { get; set; }

        public string Deleted { get; set; }

        public string DislikeAmount { get; set; }

        public string Ext { get; set; }

        public string FrontTime { get; set; }

        public string Hidden { get; set; }

        public string Id { get; set; }

        public string LikeAmount { get; set; }

        public string Mature { get; set; }

        public string Md5 { get; set; }

        public string Nsfw { get; set; }

        public string Processed { get; set; }

        public string RId { get; set; }

        public string RepostOf { get; set; }

        public string Score { get; set; }

        public string SoftDeleted { get; set; }

        public string Time { get; set; }

        public string Title { get; set; }

        public string TrendingTime { get; set; }

        public string Trust { get; set; }

        public string Type { get; set; }

        public string Url { get; set; }

        public string UserId { get; set; }

        public string Username { get; set; }

        public string Viral { get; set; }
    }
}

