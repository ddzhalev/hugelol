﻿using Hugelol.Domain.Entities;

namespace Hugelol.Domain.Interfaces
{
    public interface IUserWebScraper
    {
        User GetUser(string userId);
    }
}
