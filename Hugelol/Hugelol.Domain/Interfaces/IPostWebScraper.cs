﻿using Hugelol.Domain.Entities;
using System.Collections.Generic;

namespace Hugelol.Domain.Interfaces
{
    public interface IPostWebScraper
    {
        //gets 10 posts after one with specific id
        List<Post> GetPosts(string postId);

        Post GetPost(string postId);
    }
}
