﻿using Hugelol.Domain.Entities;
using System.Collections.Generic;

namespace Hugelol.Domain.Interfaces
{
    public interface ICategoryWebScraper
    {
        List<Category> GetCategories();
    }
}
