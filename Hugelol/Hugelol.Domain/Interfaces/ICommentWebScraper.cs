﻿using Hugelol.Domain.Entities;
using System.Collections.Generic;

namespace Hugelol.Domain.Interfaces
{
    public interface ICommentWebScraper
    {
        //gets 10 posts after one with specific id
        List<Comment> GetCommentsForPost(string postId);

        Comment GetComment(string commentId);
    }
}
