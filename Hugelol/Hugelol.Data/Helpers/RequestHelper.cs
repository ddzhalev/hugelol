﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hugelol.Data.Helpers
{
    public static class RequestHelper
    {
        public static byte[] RequestBodyBuilder(Dictionary<string, string> dataList)
        {
            var dataString = new StringBuilder();

            for (int i = 0; i < dataList.Count; i++)
            {
                if (i == 0)
                {
                    dataString.Append("{");
                }
                dataString.Append("\"" + dataList.ElementAt(i).Key + "\":\"" + dataList.ElementAt(i).Value + "\",");
                if (i == dataList.Count - 1)
                {
                    dataString.Append("}");
                }
            }

            return Encoding.UTF8.GetBytes(dataString.ToString());
        }
    }
}
