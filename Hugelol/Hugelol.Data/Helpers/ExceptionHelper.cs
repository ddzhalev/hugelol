﻿using Hugelol.Data.Models;
using System;
using System.IO;
using System.Net;

namespace Hugelol.Data.Helpers
{
    public static class ExceptionHelper
    {
        public static T SafeExecutor<T>(this object owner, Func<T> action, string actionName) where T : WebResponseModel, new()
        {
            try
            {
                return action();
            }
            catch (WebException ex)
            {
                WebException we = (WebException)ex;
                using (HttpWebResponse response = (HttpWebResponse)we.Response)
                {
                    if (response != null)
                    {
                        var exceptionMessage = "";
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            JObject jObject = JObject.Parse(error);
                            JToken jToken = jObject.GetValue("message");
                            exceptionMessage = jToken.ToString();
                        }

                        return new T
                        {
                            ResponseStatus = response.StatusCode,
                            IsSuccessful = false,
                            ErrorMessage = exceptionMessage

                        };
                    }
                    return new T
                    {
                        IsSuccessful = false,
                    };
                }
            }
            catch (Exception ex)
            {
                Log.Error($"An exception occured during ZPAM {actionName} with message {ex.Message}", ex, owner);
                return new T
                {

                    IsSuccessful = false,
                    ErrorMessage = ex.Message
                };
            }
        }
    }
}
