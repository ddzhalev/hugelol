﻿using Hugelol.Data;
using Hugelol.Data.Helpers;
using Hugelol.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Hugelol.Domain.Interfaces
{
    public class PostWebScraper : IPostWebScraper
    {
        public List<Post> GetPosts(string postId)
        {
            Func<Post> action = () =>
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                    webClient.Headers.Add("Accept", "application/json");

                    var requestData = new Dictionary<string, string>
                    {
                        {"after", postId}
                    };

                    byte[] byteResponse = webClient.UploadData(
                        Constants.hugelolFrontUrl,
                        WebRequestMethods.Http.Post,
                        RequestHelper.RequestBodyBuilder(requestData));

                    string stringResponse = Encoding.ASCII.GetString(byteResponse);
                    Post mappedResponse =
                        JsonConvert.DeserializeObject<Post>(stringResponse);
                    return mappedResponse;
                }
            };
            return this.SafeExecutor(action, "LoginUser");
        }

        public Post GetPost(string postId)
        {
            return new Post();
        }
    }
}
