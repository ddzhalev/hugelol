﻿using Hugelol.Domain.Interfaces;
using Hugelol.Domain.Entities;
using System.Collections.Generic;

namespace Hugelol.Data.Interfaces
{
    public class CategoryWebScraper : ICategoryWebScraper
    {
        public List<Category> GetCategories()
        {
            return new List<Category>();
        }
    }
}
