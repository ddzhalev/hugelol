﻿using Hugelol.Domain.Entities;
using System.Collections.Generic;

namespace Hugelol.Domain.Interfaces
{
    public class CommentWebScraper : ICommentWebScraper
    {
        //gets 10 posts after one with specific id
        public List<Comment> GetCommentsForPost(string postId)
        {
            return new List<Comment>();
        }

        public Comment GetComment(string commentId)
        {
            return new Comment();
        }
    }
}
