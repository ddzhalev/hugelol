﻿using System.Net;

namespace Hugelol.Data.Models
{
    public class WebResponseModel
    {
        public HttpStatusCode ResponseStatus { get; set; }

        public bool IsSuccessful { get; set; }

        public string ErrorMessage { get; set; }
    }
}
